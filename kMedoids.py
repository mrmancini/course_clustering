import numpy as np
import random

def kMedoids(D, k, tmax=100):
    # determine dimensions of distance matrix D
    m, n = D.shape
    M = np.sort(np.random.choice(n, k))
    M_new = np.copy(M)
    C = {}
    for t in range(tmax):
        J = np.argmin(D[:, M], axis=1)
        for kappa in range(k):
            C[kappa] = np.where(J==kappa)[0]
        for kappa in range(k):
            J = np.mean(D[np.ix_(C[kappa], C[kappa]), C[kappa]], axis=1)
            j = np.argmin(J)
            M_new[kappa] = C[kappa][j]
        np.sort(M_new)
        if np.array_equal(M, M_new):
            break
        M = np.copy(M_new)