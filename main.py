import pandas as pd
import nltk
import re
import wikipedia
import numpy as np
import csv
from nltk.stem.snowball import SnowballStemmer
import sys
import pyclust
from sklearn.metrics import pairwise_distances_argmin_min
import gensim
from sklearn.metrics.pairwise import pairwise_distances
stemmer = SnowballStemmer('english')
def tokenize_and_stem(text):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
        if re.search('[a-zA-Z]', token):
            filtered_tokens.append(token)
    stems = [stemmer.stem(t) for t in filtered_tokens]
    return stems


def group_articles(articles, titles, headings):
    num_clusters = 6
    model = gensim.models.KeyedVectors.load_word2vec_format('./GoogleNews-vectors-negative300.bin', binary=True)
    matrix = []
    for a in articles:
        arry = []
        for b in articles:
            d = model.wmdistance(a, b)
            arry.append(d)
        matrix.append(arry)
    matrix = np.array(matrix)
    matrix = matrix.T
    km = pyclust.KMedoids(n_clusters=6, n_trials=50)
    km.fit(matrix)
    clusters = km.labels_.tolist()
    data = {'heading':headings,'title': titles, 'article': articles, 'cluster': clusters}
    frame = pd.DataFrame(data, index=[clusters], columns=['heading', 'title', 'article', 'cluster'])
    #order_centroids = km.cluster_centers_.argsort()[:, ::-1]
    order_centroids = km.centers_.argsort()[:, ::-1]
    with open("cluster_output.txt", "w") as text_file:
         for i in range(num_clusters):
             print("Cluster %d titles:" % i, end='', file=text_file)
             for title in frame.ix[i]['title'].values.tolist():
                 print(' %s,' % title, end='', file=text_file)
             print("", file=text_file)
             print("Cluster %d headings:" % i, end='', file=text_file)
             for heading in frame.ix[i]['heading'].values.tolist():
                 print(' %s' % heading, end='', file=text_file)
             print("", file=text_file)
             print("", file=text_file)

    dist_matrix = pairwise_distances_argmin_min(matrix, km.centers_)
    sorted_dist_matrix = [(z, x, y) for x, y, z in sorted(zip(dist_matrix[0], dist_matrix[1], range(len(dist_matrix[0]))),reverse=True)]
    for i in range(num_clusters):
        clust = [(x,y,z) for x,y,z in sorted_dist_matrix if y == i]
        print([titles[x] for x,y,z in clust], file=text_file)


def load_data():
    file = "cmpsc_courses_with_prereqs.csv"
    articles = []
    headings = []
    titles = []
    csv.field_size_limit(sys.maxsize)
    with open(file, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter='|')
        for row in reader:
            num = float(''.join([x for x in row[0].split()[1] if x.isdigit()]))
            if num < 200:
                continue
            indx = row[1].find(re.findall('\(\d.*\)', row[1])[0])
            course_name = row[1][0:indx]
            row[1] += course_name
            row[1] += course_name
            row[1] += course_name
            course_name = re.sub('\(.*\)', '', course_name)
            course_name = re.sub('Lab', '', course_name)
            results = wikipedia.search(course_name)[0:1]
            for i in results:
                try:
                    row[1] = wikipedia.summary(i, sentences=3) + row[1]
                    #row[1] += ' '.join(wikipedia.page(i).links[0:50])
                except:
                    pass
            titles.append(row[0])
            articles.append(row[1])
            headings.append("")
    return articles, titles, headings


if __name__=="__main__":
    articles, titles, headings = load_data()
    group_articles(articles, titles, headings)





